package pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.testng.SoftAsserts;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.asserts.SoftAssert;

import properties.Url;
import utils.emailsHelper.EmailUtils;

import static com.codeborne.selenide.Configuration.AssertionMode.SOFT;
import static com.codeborne.selenide.Configuration.AssertionMode.STRICT;
import static com.codeborne.selenide.Configuration.FileDownloadMode.PROXY;

@Listeners(SoftAsserts.class)

public class BaseTest {

    protected SoftAssert softAssert;
    protected EmailUtils emailUtils;



    @BeforeSuite
    public void beforeSuite(){
        //WebDriverManager.chromedriver().version("2.26").setup(); // set specify chrome driver version
        Configuration.browser = "chrome";
        //Configuration.browserSize = "1024x768";
        Configuration.timeout = 15000;
        //Configuration.headless = true;
        Configuration.fileDownload = PROXY;  // if 'PROXY' - Selenide creates its own webdriver and starts its own proxy server for downloading files
        Configuration.reportsFolder = "target/test-result/results";
        Configuration.assertionMode = STRICT;
        Configuration.baseUrl = Url.BASE_URL;
        softAssert = new SoftAssert();


    }
}
