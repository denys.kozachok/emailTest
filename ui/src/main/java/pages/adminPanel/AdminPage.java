package pages.adminPanel;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class AdminPage {
    private  By byTabs = By.cssSelector("span");

    public SelenideElement getUsersTab(){
        return $$(byTabs).findBy(Condition.text("Users"));
    }


}
