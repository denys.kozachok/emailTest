package pages.adminPanel;

import static com.codeborne.selenide.Condition.*;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import pages.adminPanel.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;

public class UsersPage {
    private By byRow = By.cssSelector("tbody tr");
    private By byLogin = By.cssSelector("td:nth-child(1)");
    private By byName = By.cssSelector("td:nth-child(2)");
    private By bySurname = By.cssSelector("td:nth-child(3)");
    private By byEdit = By.cssSelector("td:nth-child(4) button:nth-child(1)");
    private By byDelete = By.cssSelector("td:nth-child(4) button:nth-child(2)");
    private By byPopupDeleteButtons = By.cssSelector("button span");

    public void getRow(){
        $(byRow).shouldBe(visible);
    }

    public void clickDeleteOk(){
        $$(byPopupDeleteButtons).findBy(Condition.text("Yes")).shouldBe(visible).click();
        sleep(1000);
    }

    private List<User> getUsersList(){
        List<User> users = new ArrayList<>();
        for(SelenideElement row : $$(byRow)){
            SelenideElement login = row.find(byLogin);
            SelenideElement name = row.find(byName);
            SelenideElement surname = row.find(bySurname);
            SelenideElement edit = row.find(byEdit);
            SelenideElement delete = row.find(byDelete);
            users.add(new User().
                    setLogin(login).
                    setName(name).
                    setSurname(surname).
                    setEdit(edit).
                    setDelete(delete));
        }
        return users;
    }

    public User getUserByLogin(String login){
        for(User user : getUsersList()){
            if(user.getLogin().getText().equals(login)){
                return user;
            }
        }
        return null;
    }
}
