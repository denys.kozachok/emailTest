package pages.adminPanel.model;


import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@ToString
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)

public class User {
    private SelenideElement row;
    private SelenideElement login;
    private SelenideElement name;
    private SelenideElement surname;
    private SelenideElement edit;
    private SelenideElement delete;

}
