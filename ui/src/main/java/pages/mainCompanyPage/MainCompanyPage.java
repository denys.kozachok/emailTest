package pages.mainCompanyPage;

import org.openqa.selenium.By;
import pages.monitor.coolantSystemCreation.Form_Customer;
import pages.uaa.UaaLoginPage;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static utils.PageObjectHelper.logout;
import static properties.Url.UAA_BASE_URL;

public class MainCompanyPage {

    private By plusButton = By.cssSelector("button[aria-label='add']");
    private By dashBoardLink = By.cssSelector("#home_dashboard_button");

    public Form_Customer clickPlusButton(){
        $(plusButton).click();
        return new Form_Customer();
    }



    public UaaLoginPage openLoginPage(){
        logout();
        open(UAA_BASE_URL);
        $(dashBoardLink).click();
        return new UaaLoginPage();
    }
}
