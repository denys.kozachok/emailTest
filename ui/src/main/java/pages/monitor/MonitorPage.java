package pages.monitor;

import static com.codeborne.selenide.Condition.*;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;
import pages.monitor.coolantSystemCreation.Form_Customer;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class MonitorPage {
    private By byPlusButton = By.cssSelector("button[aria-label='add']");

    public Form_Customer clickPlusButton() {
        $(byPlusButton).click();
        return new Form_Customer();
    }

    public SelenideElement getPlusButton() {
        return $(byPlusButton);
    }
}


//Tell us more about you