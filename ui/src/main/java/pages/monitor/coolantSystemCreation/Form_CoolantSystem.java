package pages.monitor.coolantSystemCreation;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Form_CoolantSystem {
    private By byTypeDropdownArrow = By.cssSelector("#coolantSystemTypeSelect+svg");
    private By byTypeInput = By.cssSelector("#coolantSystemTypeSelect");
    private By byManufecturer = By.cssSelector("#coolantSystemManufacturerLookup");
    private By byManufecturerFirstDropDownOption = By.cssSelector("#coolantSystemManufacturerLookup");



    public Form_CoolantSystem selectType(String type){
        $(byTypeDropdownArrow).click();
        String newType = type.toUpperCase();
        $(byTypeInput).setValue(newType);
        return this;
    }

    public Form_CoolantSystem setManufecturer(String manufecturer){
        $(byManufecturer).click();
        //$(byManufecturer).parent()
return this;
    }
}
