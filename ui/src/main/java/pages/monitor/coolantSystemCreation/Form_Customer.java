package pages.monitor.coolantSystemCreation;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Form_Customer {

    private By byCustomerInput = By.id("wizardCustomerLookup");


    public Form_HeaderAndFooter inputCustomer(String customer){
        $(byCustomerInput).setValue(customer);
        $(byCustomerInput).parent().click();
        return new Form_HeaderAndFooter();
    }



}
