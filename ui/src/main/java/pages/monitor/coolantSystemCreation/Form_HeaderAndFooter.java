package pages.monitor.coolantSystemCreation;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class Form_HeaderAndFooter {
    private By byCancelButton = By.id("wizardCancelButton");
    private By byBackButton = By.id("wizardBackButton");
    private By byNextButton = By.id("wizardNextButton");

    public void clickNextButton(){
         $(byNextButton).shouldBe(visible).click();
    }
}
