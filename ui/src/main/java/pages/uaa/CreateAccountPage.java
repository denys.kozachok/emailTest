package pages.uaa;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CreateAccountPage {

    private By byEmail = By.cssSelector("input[name = 'email']");
    private By byPassword = By.cssSelector("input[name = 'password']");
    private By byConfirm = By.cssSelector("input[name = 'password_confirmation']");
    private By byAgreeCheckbox = By.cssSelector("input[name = 'does_user_consent']");
    private By bySendActivationLink = By.cssSelector("input[type = 'submit']");

    public void fillForm(String email, String password){
        $(byEmail).setValue(email);
        $(byPassword).setValue(password);
        $(byConfirm).setValue(password);
        $(byAgreeCheckbox).click();
        $(bySendActivationLink).click();

    }
}
