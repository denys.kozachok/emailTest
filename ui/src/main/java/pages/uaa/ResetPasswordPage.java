package pages.uaa;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ResetPasswordPage {
    private By byEmail = By.cssSelector("input[name=\"username\"]");
    private By byPassword = By.cssSelector("input[name=\"password\"]");
    private By byPasswordConfirm = By.cssSelector("input[name=\"password_confirmation\"]");
    private By bySubmitButton = By.cssSelector("input[type=\"submit\"]");

    public void sendEmail(String email) {
        $(byEmail).sendKeys(email);
        $(bySubmitButton).click();
    }

    public void applyNewPassword(String password) {
        $(byPassword).sendKeys(password);
        $(byPasswordConfirm).sendKeys(password);
        $(bySubmitButton).click();
        }
}
