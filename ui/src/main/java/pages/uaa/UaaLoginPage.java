package pages.uaa;

import static com.codeborne.selenide.Condition.*;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class UaaLoginPage {

    private By byLogin = By.cssSelector("input[name='username']");
    private By byPassword = By.cssSelector("input[name='password']");
    private By bySubmit = By.cssSelector("input[type='submit']");
    private By byCreateAccountLink = By.cssSelector(".left");
    private By byResetPasswordLink = By.cssSelector(".right");
    private By byAlertError = By.cssSelector(".alert.alert-error");
    private By byAlertSuccess = By.cssSelector(".alert.alert-success");

    @Step("Fill login form")
    public void doLogin(String login, String password){
        $(byLogin).setValue(login);
        $(byPassword).setValue(password);
        $(bySubmit).click();
    }

    @Step("Click 'Create Account'")
    public CreateAccountPage clickCreateAccount(){
        $(byCreateAccountLink).click();
        return new CreateAccountPage();
    }

    @Step("Click 'Reset Password'")
    public ResetPasswordPage clickResetPassword(){
        $(byResetPasswordLink).click();
        return new ResetPasswordPage();
    }


    public SelenideElement getAlertError(){
        return $(byAlertError);
    }
    public SelenideElement getAlertSuccess(){
        return $(byAlertSuccess);
    }

    public SelenideElement getLogin(){ return $(byLogin); }
    public SelenideElement getPassword(){ return $(byPassword); }
    public SelenideElement getSubmit(){ return $(bySubmit); }
    public SelenideElement getCreateAccountLink(){ return $(byCreateAccountLink); }
    public SelenideElement getResetPasswordLink(){ return $(byResetPasswordLink); }




    public boolean isAlertAppears(){
        return $(byAlertError).is(visible);
    }


}
