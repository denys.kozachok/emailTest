package properties;

public class Cred {
    public final static String ADMIN_LOGIN = PropertyLoader.loadCredProperty("adminLogin");
    public final static String ADMIN_PASSWORD = PropertyLoader.loadCredProperty("adminPassword");
    public final static String SALES_MANAGER_LOGIN = PropertyLoader.loadCredProperty("salesManagerLogin");
    public final static String SALES_MANAGER_PASSWORD = PropertyLoader.loadCredProperty("salesManagerPassword");
    public final static String SALES_ENGINEER_LOGIN = PropertyLoader.loadCredProperty("salesEngineerLogin");
    public final static String SALES_ENGINEER_PASSWORD = PropertyLoader.loadCredProperty("salesEngineerPassword");
    public final static String DISTRIBUTOR_LOGIN = PropertyLoader.loadCredProperty("distributorLogin");
    public final static String DISTRIBUTOR_PASSWORD = PropertyLoader.loadCredProperty("distributorPassword");
    public final static String DISTRIBUTOR_MNG_LOGIN = PropertyLoader.loadCredProperty("distributorMngLogin");
    public final static String DISTRIBUTOR_MNG_PASSWORD = PropertyLoader.loadCredProperty("distributorMngPassword");
    public final static String DISTRIBUTOR_ENG_LOGIN = PropertyLoader.loadCredProperty("distributorEngLogin");
    public final static String DISTRIBUTOR_ENG_PASSWORD = PropertyLoader.loadCredProperty("distributorEngPassword");
    public final static String CUSTOMER_LOGIN = PropertyLoader.loadCredProperty("customerLogin");
    public final static String CUSTOMER_PASSWORD = PropertyLoader.loadCredProperty("customerPassword");
    public final static String CUSTOMER_USER_LOGIN = PropertyLoader.loadCredProperty("customerUserLogin");
    public final static String CUSTOMER_USER_PASSWORD = PropertyLoader.loadCredProperty("customerUserPassword");


    public final static String BASIC_AUTH_LOGIN = PropertyLoader.loadCredProperty("basicAuthLogin");
    public final static String BASIC_AUTH_PASSWORD = PropertyLoader.loadCredProperty("basicAuthPassword");
    public static String TEST_EMAIL = PropertyLoader.loadEmailProperty("mail.smtp.user");
    public static String TEST_EMAIL_PASSWORD = PropertyLoader.loadEmailProperty("mail.smtp.password");
    public static String TEST_EMAIL_SERVER = PropertyLoader.loadEmailProperty("mail.smtp.host");









}
