package properties;

public enum PropertySource {
    URL("properties/url.properties"),
    CRED("properties/cred.properties"),
    MAIL("properties/email.properties");

    public String sourceFile;

    public PropertySource[] getOptions(){
        return PropertySource.values();
    }

    PropertySource(String sourceFile) {
        this.sourceFile = sourceFile;
    }

}
