package properties;

public class Url {
    public static String BASE_URL = PropertyLoader.loadUrlProperty("base_url");
    public static String UAA_BASE_URL = PropertyLoader.loadUrlProperty("uaaBaseUrl");
    public static String UAA_LOGIN_PATH = PropertyLoader.loadUrlProperty("loginrUrl");
    public static String ANALYZER = PropertyLoader.loadUrlProperty("analyzer");
    public static String DASHBOARD = PropertyLoader.loadUrlProperty("dashboard");
    public static String MONITOR = PropertyLoader.loadUrlProperty("monitor");
    public static String ADMIN_PANEL = PropertyLoader.loadUrlProperty("adminPanel");
    public static String BASIC_AUTHORIATION = PropertyLoader.loadUrlProperty("basicAuthorization");
    public static String CREATE_ACCOUNT= PropertyLoader.loadUrlProperty("createAccount");
}
