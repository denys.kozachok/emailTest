package properties;

public enum UsersRoles {
    ADMIN,
    SALES_MANAGER,
    SALES_ENGINEER,
    DISTRIBUTOR_MNG,
    DISTRIBUTOR_ENG,
    CUSTOMER,
    CUSTOMER_USER;
}
