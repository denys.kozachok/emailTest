package utils;

import com.codeborne.selenide.WebDriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import static org.openqa.selenium.firefox.FirefoxDriver.PROFILE;

public class CustomIncognitoDriver implements WebDriverProvider {
    //@Override
    public WebDriver createDriver(DesiredCapabilities capabilities) {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver");
        capabilities.acceptInsecureCerts();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        capabilities.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true); //about a problem with 'self-sighned' sertificat - http://ru.selenide.org/2015/06/21/selenide-2.19/
        //options.setCapability("network.http.phishy-userpass-length", 255);
        return new ChromeDriver(options);
    }
}
