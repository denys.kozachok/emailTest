package utils;

import org.testng.annotations.*;
import properties.PropertyLoader;

import static properties.Cred.*;

public class DataProvider {

    @org.testng.annotations.DataProvider
    public static Object[][] adminPanelRolesAccess(){
        return new Object[][]{
                {ADMIN_LOGIN, ADMIN_PASSWORD, true},
                {SALES_MANAGER_LOGIN, SALES_MANAGER_PASSWORD, false},
                {SALES_ENGINEER_LOGIN, SALES_ENGINEER_PASSWORD, false},
                {DISTRIBUTOR_LOGIN, DISTRIBUTOR_PASSWORD, true},
                {DISTRIBUTOR_MNG_LOGIN, DISTRIBUTOR_MNG_PASSWORD, false},
                {DISTRIBUTOR_ENG_LOGIN, DISTRIBUTOR_ENG_PASSWORD, false},
                {CUSTOMER_LOGIN, CUSTOMER_PASSWORD, true},
                {CUSTOMER_USER_LOGIN, CUSTOMER_USER_PASSWORD, false}
        };
    }
}
