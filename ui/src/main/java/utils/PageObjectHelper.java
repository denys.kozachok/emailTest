package utils;

import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selenide.sleep;

public class PageObjectHelper {

    public static void logout() {
        WebDriverRunner.getWebDriver().manage().deleteAllCookies();
    }

    public static void waitFullPageLoading() {
        System.out.println("Wating for ready state complete");
        JavascriptExecutor js = (JavascriptExecutor) WebDriverRunner.getWebDriver();
        String readyState = js.executeScript("return document.readyState").toString();
        System.out.println("Ready State: " + readyState);
        while(!readyState.equals("complete")){
            sleep(2000);
        }

    }

}
