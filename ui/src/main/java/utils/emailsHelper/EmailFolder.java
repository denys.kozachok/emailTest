package utils.emailsHelper;

public enum EmailFolder {

    INBOX("INBOX"),
    SPAM("[Gmail]/Спам");

    private String text;

    EmailFolder(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}
