package utils.emailsHelper;

import properties.PropertyLoader;

import javax.mail.*;
import javax.mail.search.SubjectTerm;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

import static com.codeborne.selenide.Selenide.sleep;
import static properties.Cred.TEST_EMAIL;
import static properties.Cred.TEST_EMAIL_PASSWORD;

public class EmailUtils {

    /**
     * Source - http://angiejones.tech/test-automation-to-verify-email
     */
    int count = 0;
    private Folder folder;
    String inviteMessage = "Activate your Liquid Tool account";
    String resetPasswordMessage = "Liquid Tool account password reset request";

    public EmailUtils(EmailFolder emailFolder) throws MessagingException {
        this(getEmailUsernameFromProperties(),
                getEmailPasswordFromProperties(),
                getEmailServerFromProperties(),
                emailFolder);
    }

    /**
     * Connects to email server with credentials provided to read from a given folder of the email application
     *    /Users/andrew/Documents/Automation/Lanars/LiquidTools/ui/src/main/resources/properties/email.properties
     * @param username    Email username (e.g. janedoe@email.com)
     * @param password    Email password
     * @param server      Email server (e.g. smtp.email.com)
     * @param emailFolder Folder in email application to interact with
     */
    public EmailUtils(String username, String password, String server, EmailFolder emailFolder) throws MessagingException {
        Properties props = System.getProperties();
        try {
            props.load(new FileInputStream(new File("../ui/src/main/resources/properties/email.properties")));
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        Session session = Session.getInstance(props, null);
        Store store = session.getStore("imaps");
        store.connect(server, username, password);

        folder = store.getFolder(emailFolder.getText());
        folder.open(Folder.READ_WRITE);
    }


    public static String getEmailUsernameFromProperties() {
        return PropertyLoader.loadEmailProperty("mail.smtp.user");
    }
    public static String getEmailPasswordFromProperties() {
        return PropertyLoader.loadEmailProperty("mail.smtp.password");
    }
    public static String getEmailServerFromProperties() {
        return PropertyLoader.loadEmailProperty("mail.smtp.host");
    }

    public String getRegistrationLink() {

        try {
            sleep(2000);    //need to wait while email will come
            Message email = getMessagesBySubject(inviteMessage, true, 1)[0];
            BufferedReader reader = new BufferedReader(new InputStreamReader(email.getInputStream()));
            String line;
            String link = "Activate your account";

            while ((line = reader.readLine()) != null) {
                if (line.contains(link)) {
                    return line.substring(line.indexOf("http"), line.indexOf("\">"));
                }
            }
        } catch (Exception e) {
            if(count<7){                    //wait up to 7 seconds for email to come
                sleep(1000);
                count++;
                getRegistrationLink();
            }
            e.printStackTrace();
        }
        return null;
    }

    public String getPasswordRecoveryLink() {

        try {
            sleep(2000);    //need to wait while email will come
            Message email = getMessagesBySubject(resetPasswordMessage, true, 1)[0];
            BufferedReader reader = new BufferedReader(new InputStreamReader(email.getInputStream()));
            String line;
            String link = "Reset your password";

            while ((line = reader.readLine()) != null) {
                if (line.contains(link)) {
                    return line.substring(line.indexOf("http"), line.indexOf("\">"));
                }
            }
        } catch (Exception e) {
            if(count<7){                    //wait up to 7 seconds for email to come
                sleep(1000);
                count++;
                getRegistrationLink();
            }
            e.printStackTrace();
        }
        return null;
    }

    public Message[] getMessagesBySubject(String subject, boolean unreadOnly, int maxToSearch) throws Exception {
        Map<String, Integer> indices = getStartAndEndIndices(maxToSearch);

        Message messages[] = folder.search(
                new SubjectTerm(subject),
                folder.getMessages(indices.get("startIndex"), indices.get("endIndex")));

        if (unreadOnly) {
            List<Message> unreadMessages = new ArrayList<Message>();
            for (Message message : messages) {
                if (isMessageUnread(message)) {
                    unreadMessages.add(message);
                }
            }
            messages = unreadMessages.toArray(new Message[]{});
        }

        return messages;
    }

    private Map<String, Integer> getStartAndEndIndices(int max) throws MessagingException {
        int endIndex = getNumberOfMessages();
        int startIndex = endIndex - max;

        //In event that maxToGet is greater than number of messages that exist
        if (startIndex < 1) {
            startIndex = 1;
        }

        Map<String, Integer> indices = new HashMap<String, Integer>();
        indices.put("startIndex", startIndex);
        indices.put("endIndex", endIndex);

        return indices;
    }

    public int getNumberOfMessages() throws MessagingException {
        return folder.getMessageCount();
    }

    public boolean isMessageUnread(Message message) throws Exception {
        return !message.isSet(Flags.Flag.SEEN);
    }

    public static String getEmailProtocolFromProperties() {
        return PropertyLoader.loadEmailProperty("mail.transport.protocol");
    }

    public static int getEmailPortFromProperties() {
        return Integer.parseInt(PropertyLoader.loadEmailProperty("mail.smtp.port"));
    }

    public void printGmailFolders(){
        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");
        try {
            Session session = Session.getDefaultInstance(props, null);
            javax.mail.Store store = session.getStore("imaps");
            store.connect("imap.gmail.com", TEST_EMAIL, TEST_EMAIL_PASSWORD);
            javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
            for (javax.mail.Folder folder : folders) {
                if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {
                    System.out.println(folder.getFullName() + ": " + folder.getMessageCount());
                }
            }
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
