package loginTests;

import static com.codeborne.selenide.Selenide.open;
import static properties.Cred.BASIC_AUTH_LOGIN;
import static properties.Cred.BASIC_AUTH_PASSWORD;
import static properties.Url.ADMIN_PANEL;
import static properties.Url.UAA_BASE_URL;
import static properties.Url.UAA_LOGIN_PATH;
import static utils.PageObjectHelper.logout;

public class TestsHelper {

    public void openLoginPage(){
        logout(); //clean cookies
        open(ADMIN_PANEL,"", BASIC_AUTH_LOGIN,BASIC_AUTH_PASSWORD);
        open(UAA_BASE_URL + UAA_LOGIN_PATH);
    }


}
