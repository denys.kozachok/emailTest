package loginTests;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.BaseTest;
import pages.adminPanel.AdminPage;
import pages.adminPanel.UsersPage;
import pages.monitor.MonitorPage;
import pages.uaa.ResetPasswordPage;
import pages.uaa.UaaLoginPage;
import ru.yandex.qatools.allure.annotations.Title;
import utils.emailsHelper.EmailUtils;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static properties.Cred.*;
import static properties.Url.*;
import static utils.PageObjectHelper.logout;
import static utils.emailsHelper.EmailFolder.SPAM;

public class UaaTests extends BaseTest {

    private UaaLoginPage uaaLoginPage;
    private TestsHelper testsHelper;
    private MonitorPage monitorPage;
    private AdminPage adminPage;
    private ResetPasswordPage resetPasswordPage;
    private UsersPage usersPage;

    @BeforeClass
    public void setUp() throws Exception {
        adminPage = new AdminPage();
        usersPage = new UsersPage();
        uaaLoginPage = new UaaLoginPage();
        testsHelper = new TestsHelper();
        monitorPage = new MonitorPage();
        resetPasswordPage = new ResetPasswordPage();
        emailUtils = new EmailUtils(TEST_EMAIL, TEST_EMAIL_PASSWORD, TEST_EMAIL_SERVER, SPAM); //connect to smtp server
    }

    @Severity(SeverityLevel.NORMAL)
    @Title("Login page should contains all necessary elements")
    @Description("Login page should contains all elements")
    @Test
    public void uaaPageContainsAllRequiredElements() {
        testsHelper.openLoginPage();
        uaaLoginPage.getLogin().shouldBe(visible);
        uaaLoginPage.getPassword().shouldBe(visible);
        uaaLoginPage.getSubmit().shouldBe(visible);
        uaaLoginPage.getCreateAccountLink().shouldBe(visible);
        uaaLoginPage.getResetPasswordLink().shouldBe(visible);
    }

    @Severity(SeverityLevel.BLOCKER)
    @Title("Check, if tests can make login into Monitor")
    @Description("Check, if tests can make login into Monitor")
    @Test
    public void verifyUserCanLoginIntoMonitor() {
        logout();
        open(BASE_URL,"", "lts","LTS!34");
        open(MONITOR);
        uaaLoginPage.doLogin(ADMIN_LOGIN, ADMIN_PASSWORD);
        monitorPage.getPlusButton().shouldBe(Condition.visible);//check another elelemnts for default visibility
    }


//TODO make user deletion from DB
    @Severity(SeverityLevel.BLOCKER)
    @Title("Check, if new user can be created")
    @Description("Check, if new user can be created")
    @Test
    public void verifyCreateAccountFormCreateNewUser(){
        testsHelper.openLoginPage();
        uaaLoginPage.clickCreateAccount().fillForm(TEST_EMAIL, TEST_EMAIL_PASSWORD);
        String registrationUrl = emailUtils.getRegistrationLink();
        open(registrationUrl);
        uaaLoginPage.getAlertSuccess().shouldBe(visible);
        uaaLoginPage.doLogin(TEST_EMAIL, TEST_EMAIL_PASSWORD);
        Assert.assertTrue(url().contains("liquidtool.io/uaa"));
        //--------------User deletion from Admin Panel---------------
        testsHelper.openLoginPage();
        uaaLoginPage.doLogin(ADMIN_LOGIN, ADMIN_PASSWORD);
        open(ADMIN_PANEL, "", BASIC_AUTH_LOGIN, BASIC_AUTH_PASSWORD);
        adminPage.getUsersTab().should(visible).click();
        //TODO delete for loop after bug with deletion will be fixed
        for(int i=0; i<2; i++){
            usersPage.getRow();
            usersPage.getUserByLogin(TEST_EMAIL).getDelete().shouldBe(visible).click();
            usersPage.clickDeleteOk();
        }
        Assert.assertNull(usersPage.getUserByLogin(TEST_EMAIL));

    }

    @Severity(SeverityLevel.BLOCKER)
    @Title("Check, if user can reset password")
    @Description("Check, if user can reset password")
    @Test
    public void verifyUserCanResetPassword (){
//------------Remove this block after it will be possible to create test user, need to use test user credentials-------------
        testsHelper.openLoginPage();
        uaaLoginPage.clickCreateAccount().fillForm(TEST_EMAIL, TEST_EMAIL_PASSWORD);
        String registrationUrl = emailUtils.getRegistrationLink();
        open(registrationUrl);
        uaaLoginPage.getAlertSuccess().shouldBe(visible);
        uaaLoginPage.doLogin(TEST_EMAIL, TEST_EMAIL_PASSWORD);
        Assert.assertTrue(url().contains("liquidtool.io/uaa"));
//-----------------------------------------------------------------------------------------
        testsHelper.openLoginPage();
        uaaLoginPage.clickResetPassword().sendEmail(TEST_EMAIL);
        String passwordRecoveryUrlUrl = emailUtils.getPasswordRecoveryLink();
        open(passwordRecoveryUrlUrl);
        resetPasswordPage.applyNewPassword(TEST_EMAIL_PASSWORD + "1");
        uaaLoginPage.getAlertSuccess().shouldBe(visible);
        uaaLoginPage.doLogin(TEST_EMAIL, TEST_EMAIL_PASSWORD + "1");
        Assert.assertTrue(url().contains("liquidtool.io/uaa"));
        //--------------User deletion from Admin Panel---------------
        testsHelper.openLoginPage();
        uaaLoginPage.doLogin(ADMIN_LOGIN, ADMIN_PASSWORD);
        open(ADMIN_PANEL, "", BASIC_AUTH_LOGIN, BASIC_AUTH_PASSWORD);
        adminPage.getUsersTab().should(visible).click();
        //TODO delete for loop after bug with deletion will be fixed
        for(int i=0; i<2; i++){
            usersPage.getRow();
            usersPage.getUserByLogin(TEST_EMAIL).getDelete().shouldBe(visible).click();
            usersPage.clickDeleteOk();
        }
        Assert.assertNull(usersPage.getUserByLogin(TEST_EMAIL));

    }



}
